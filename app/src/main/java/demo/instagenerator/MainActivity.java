package demo.instagenerator;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.os.EnvironmentCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity {

    private static final int PICK_IMAGE_REQUEST = 0;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1;
    private Button mBtnSelect;
    private Paint mPaint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mPaint = new Paint();
        mPaint.setColor(Color.WHITE);
        mPaint.setTextSize(100);

        mBtnSelect = (Button)findViewById(R.id.btnSelect);


        mBtnSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= 23){
                    // Here, thisActivity is the current activity
                    if (ContextCompat.checkSelfPermission(MainActivity.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {

                        // Should we show an explanation?
                        if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                                Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            Toast.makeText(MainActivity.this, "asdfasdfasdf", Toast.LENGTH_SHORT).show();

                        } else {
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    }else{
                        ActivityCompat.requestPermissions(MainActivity.this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    }
                }else {
                    selectPhoto();
                }
            }
        });
    }

    private void selectPhoto() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    selectPhoto();
                } else {
                }
                return;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                //Display an error
                return;
            }

            try {
                InputStream inputStream = getContentResolver().openInputStream(data.getData());
                Bitmap bitmapOld = BitmapFactory.decodeStream(inputStream);
                Bitmap bitmap = bitmapOld.copy(bitmapOld.getConfig(), true);
                bitmapOld.recycle();
                Canvas canvas = new Canvas(bitmap);
                canvas.drawText("Молодой человек", 0, bitmap.getHeight() / 2, mPaint);
                canvas.drawText("да вы же", 0, bitmap.getHeight() / 2 + 100, mPaint);
                canvas.drawText("пидор!", 0, bitmap.getHeight() / 2 + 200, mPaint);
                File file = new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES), "test_generated.png");
                FileOutputStream fos = new FileOutputStream(file);
                boolean result = bitmap.compress(Bitmap.CompressFormat.PNG, 80, fos);
                if (!result){
                    Toast.makeText(this, "Не удалось сохранить", Toast.LENGTH_SHORT).show();
                    return;
                }
                fos.close();
                String type = "image/*";
                Intent share = new Intent(Intent.ACTION_SEND);
                share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                Uri contentUri = Uri.fromFile(file);
                share.setDataAndType(contentUri, getContentResolver().getType(contentUri));
                share.setType(type);
                share.putExtra(Intent.EXTRA_STREAM, contentUri);
                //share.setPackage("com.instagram.android");
                startActivity(Intent.createChooser(share, "Choose your destiny"));
            } catch (FileNotFoundException e) {
                Log.e("ERROR", "Error", e);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
